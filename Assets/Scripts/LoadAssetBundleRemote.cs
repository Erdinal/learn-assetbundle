using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class LoadAssetBundleRemote : MonoBehaviour
{
    [SerializeField] private GameObject _targetObject;
    [SerializeField] private string _bundleUrl;
    [SerializeField] private string _bundleName;

    private AssetBundle downloadedBundle;

    private System.Action OnDownloadComplete;

    void Start()
    {
        if (_targetObject == null) return;

        OnDownloadComplete += () => AssignTextureBundle(LoadTextureFromBundleFile());
        StartCoroutine(DownloadBundle());
    }

    private IEnumerator DownloadBundle()
    {
        UnityWebRequest webRequest = UnityWebRequestAssetBundle.GetAssetBundle(_bundleUrl);

        yield return webRequest.SendWebRequest();

        AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(webRequest);
        downloadedBundle = bundle;

        OnDownloadComplete.Invoke();
    }

    private Texture LoadTextureFromBundleFile()
    {
        return downloadedBundle.LoadAsset<Texture>(_bundleName);
    }

    private void AssignTextureBundle(Texture newTexture)
    {
        _targetObject.GetComponent<MeshRenderer>().materials[0].SetTexture("_MainTex", newTexture);
    }
}
