using System.IO;
using UnityEngine;

public class LoadAssetBundleLocal : MonoBehaviour
{
    [SerializeField] private GameObject _targetObject;
    [SerializeField] private string _bundlePath;
    [SerializeField] private string _bundleName;

    private void Start()
    {
        if (_targetObject == null) return;

        Texture bundleTexture = LoadTextureFromBundleFile();
        AssignTextureBundle(bundleTexture);
    }

    private Texture LoadTextureFromBundleFile()
    {
        var loadedBundle = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, _bundlePath));

        if (loadedBundle == null) return null;

        Texture texture = loadedBundle.LoadAsset<Texture>(_bundleName);

        return texture;
    }

    private void AssignTextureBundle(Texture newTexture)
    {
        _targetObject.GetComponent<MeshRenderer>().materials[0].SetTexture("_MainTex", newTexture);
    }
}
